package com.eilco.davidregnard.lolme;

import android.widget.ImageView;
import com.bumptech.glide.Glide;

public class DDragon {

    public static final String ENDPOINT = "http://ddragon.leagueoflegends.com/cdn/8.24.1";

    public void setProfileIcon(int profileIconId,ImageView iv){
        Glide
            .with(iv)
            .load(ENDPOINT+"/img/profileicon/"+profileIconId+".png")
            .into(iv);
    }
}