package com.eilco.davidregnard.lolme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.eilco.davidregnard.lolme.RetrofitService.APIKey;
import static com.eilco.davidregnard.lolme.RetrofitService.ENDPOINT;

public class InfoActivity extends AppCompatActivity {

    private TextView name;
    private ImageView profileIcon;
    private TextView id;
    private TextView summonerLevel;
    private TextView revisionDate;
    private TextView accountId;

    private TextView soloRank;
    private TextView flexRank;
    private TextView vs3Rank;

    private ArrayList<Position> listPositions;
    private ArrayList<Match> listMatch;

    private RecyclerView matchesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DDragon DDragon = new DDragon();
        setContentView(R.layout.activity_info);
        Summoner summoner = (Summoner) getIntent().getSerializableExtra("summoner");
        name = findViewById(R.id.name);
        profileIcon = findViewById(R.id.profileIcon);
    //    id = findViewById(R.id.id);
        summonerLevel = findViewById(R.id.summonerLevel);
    //   revisionDate = findViewById(R.id.revisionDate);
    //    accountId = findViewById(R.id.accountId);


        DDragon.setProfileIcon(summoner.getProfileIconId(),profileIcon);
   //     id.setText(Long.toString(summoner.getId()));
        summonerLevel.setText(Long.toString(summoner.getSummonerLevel()));
        name.setText(summoner.getName());
    //    revisionDate.setText(Long.toString(summoner.getRevisionDate()));
   //accountId.setText(Long.toString(summoner.getAccountId()));
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofit.create(RetrofitService.class);
        final RetrofitService service = retrofit.create(RetrofitService.class);
        service.getPositionsBySummonerID(summoner.getId(),APIKey).enqueue(new Callback<ArrayList<Position>>() {
            @Override
            public void onResponse(Call<ArrayList<Position>> call, Response<ArrayList<Position>> response) {
                listPositions = response.body();
            }

            @Override
            public void onFailure(Call<ArrayList<Position>> call, Throwable t) {
                listPositions = null;
            }
        });

        service.getMatchesByAccountID(summoner.getAccountId(),APIKey).enqueue(new Callback<ArrayList<Match>>() {
            @Override
            public void onResponse(Call<ArrayList<Match>> call, Response<ArrayList<Match>> response) {
                listMatch = response.body();
            }

            @Override
            public void onFailure(Call<ArrayList<Match>> call, Throwable t) {
                listMatch = null;
            }
        });

        soloRank = findViewById(R.id.soloRank);
        flexRank = findViewById(R.id.flexRank);
        vs3Rank = findViewById(R.id.vs3Rank);

        for(Position p : listPositions) {
            switch (p.getQueueType()) {
                case "RANKED_SOLO_5x5":
                    soloRank.setText(p.getTier() + " " + p.getRank());
                    break;
                case "RANKED_FLEX_SR":
                    flexRank.setText(p.getTier() + " " + p.getRank());
                    break;
                case "RANKED_FLEX_TT":
                    vs3Rank.setText(p.getTier() + " " + p.getRank());
                    break;
                default:
                    break;
            }
        }

    }
}
