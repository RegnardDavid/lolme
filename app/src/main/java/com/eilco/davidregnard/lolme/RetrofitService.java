package com.eilco.davidregnard.lolme;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {

    public static final String ENDPOINT = "https://euw1.api.riotgames.com";

    public static final String APIKey = "RGAPI-daab4c7f-736f-4efd-b5eb-ac93a92ba7bf";

    @GET("/lol/summoner/v3/summoners/by-name/{summonerName}")
    Call<Summoner> getSummoner(@Path("summonerName") String summonerName, @Query("api_key") String ApiKey);

    @GET("/lol/league/v3/positions/by-summoner/{summonerId}")
    Call<ArrayList<Position>> getPositionsBySummonerID(@Path("summonerId") long summonerId, @Query("api_key") String ApiKey);

    @GET("/lol/match/v3/matchlists/by-account/{accountId}")
    Call<ArrayList<Match>> getMatchesByAccountID(@Path("accountId") long summonerId, @Query("api_key") String ApiKey);
}